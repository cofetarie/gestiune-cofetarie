﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneCofetarie.Utilities
{
    class GridColumnIesire
    {
        public Iesire iesire;
        public string Ingredient
        {
            get; set;
        }
        public float Cantitate
        {
            get; set;
        }
        public decimal PretUnitar
        {
            get; set;
        }
        public DateTime Data
        {
            get; set;
        }

        public GridColumnIesire()
        {

        }
        public GridColumnIesire(string ingredient, Iesire iesire)
        {
            this.iesire = iesire;
            Ingredient = ingredient;
            Cantitate = iesire.Cantitate;
            PretUnitar = iesire.PretUnitar;
            Data = iesire.Data;
        }
    }
}
