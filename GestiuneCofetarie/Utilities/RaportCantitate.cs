﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneCofetarie.Utilities
{
    class RaportCantitate
    {
        public IngredientCompozitie Ingredient { get; set; }
        public float CantitateInitiala{get;set;}
        public float CantitateNecesara { get; set; }

        public RaportCantitate() { }

        public RaportCantitate(IngredientCompozitie ingredient, float cantitateInitiala, float cantitateNecesara)
        {
            Ingredient = ingredient;
            CantitateInitiala = cantitateInitiala;
            CantitateNecesara = cantitateNecesara;
        }
    }
}
