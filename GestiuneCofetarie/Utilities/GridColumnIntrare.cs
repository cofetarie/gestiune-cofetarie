﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneCofetarie.Utilities
{
    class GridColumnIntrare
    {
        public IntrareAprovizionare intrare;
        public string Ingredient
        {
            get;set;
        }
        public float Cantitate
        {
            get;set;
        }
        public decimal PretUnitar
        {
            get;set;
        }
        public DateTime Data
        {
            get;set;
        }

        public GridColumnIntrare()
        {

        }
        public GridColumnIntrare(string ingredient, IntrareAprovizionare intrare)
        {
            this.intrare = intrare;
            Ingredient = ingredient;
            Cantitate = intrare.Cantitate;
            PretUnitar = intrare.PretUnitar;
            Data = intrare.Data;
        }

    }
}
