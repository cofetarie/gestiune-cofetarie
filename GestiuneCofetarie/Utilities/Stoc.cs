﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneCofetarie.Utilities
{
    public class Stoc
    {
        public string Ingredient
        {
            get;set;
        }
        public float Cantitate
        {
            get;set;
        }

        public Stoc()
        {
            
        }
        public Stoc(string ingredient, float cantitate)
        {
            Ingredient = ingredient;
            Cantitate = cantitate;
        }
    }
}
