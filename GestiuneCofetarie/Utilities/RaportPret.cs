﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneCofetarie.Utilities
{
    class RaportPret
    {
        public IngredientCompozitie Ingredient { get; set; }
        public float Cantitate { get; set; }
        public float CantitateNecesara { get; set; }
        public decimal Pret { get; set; }

        public RaportPret() { }
        public RaportPret(IngredientCompozitie ingredient, float raport)
        {
            Ingredient = ingredient;
            Cantitate = ingredient.Cantitate;
            CantitateNecesara = raport * ingredient.Cantitate;
            CofetarieEntities cofetarie = new CofetarieEntities();
            decimal pret = 0;
            float k = 0;
            foreach (var i in cofetarie.Intrares)
                if (i.IdIngredient == ingredient.IdIngredient)
                {
                    pret = ((decimal)k * pret + (decimal)i.Cantitate * i.PretUnitar) / (decimal)(k + i.Cantitate);
                    k += i.Cantitate;
                }
            Pret = pret * (decimal)raport;
        }
    }
}
