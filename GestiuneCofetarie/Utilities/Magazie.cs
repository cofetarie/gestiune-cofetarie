﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneCofetarie.Utilities
{
    class Magazie
    {
        private Intrare intrare;
        private Iesire iesire;
        public Magazie(Intrare intrare, float stocInitial, float CMPinitial)
        {
            this.intrare = intrare;
            iesire = null;
            Data = intrare.Data;
            Intrare = intrare.Cantitate;
            Stoc = stocInitial + intrare.Cantitate;
            PretUnitar = intrare.PretUnitar;
            CMP = (stocInitial * CMPinitial + intrare.Cantitate * (float)intrare.PretUnitar) / (stocInitial + intrare.Cantitate);
        }
        public Magazie(Iesire iesire, float stocInitial, float CMPinitial)
        {
            this.iesire = iesire;
            intrare = null;
            Data = iesire.Data;
            Iesire = iesire.Cantitate;
            Stoc = stocInitial - iesire.Cantitate;
            CMP = CMPinitial;
        }

        public Magazie()
        {
        }
        public DateTime Data { get; set; }

        public float Intrare { get; set; }

        public float Iesire {get;set;}

        public float Stoc { get; set; }

        public decimal PretUnitar { get; set; }

        public float CMP { get; set; }

    }
}
