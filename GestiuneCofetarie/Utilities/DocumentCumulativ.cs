﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneCofetarie.Utilities
{
    class DocumentCumulativ
    {
        public Ingredient Denumire { get; set; }
        public float Cantitate { get; set; }
        public decimal PretUnitar { get; set; }
        public decimal PretTotal { get; set; }

        public DocumentCumulativ() { }
        public DocumentCumulativ(Iesire iesire)
        {
            CofetarieEntities cofetarie = new CofetarieEntities();
            Ingredient ingredient = cofetarie.Ingredients.Find(iesire.IdIngredient);
            Denumire = ingredient;
            Cantitate = iesire.Cantitate;
            PretUnitar = iesire.PretUnitar;
            PretTotal = (decimal) Cantitate * PretUnitar;
        }
    }
}
