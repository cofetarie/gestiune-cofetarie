﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestiuneCofetarie
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CofetarieEntities cofetarie;
        private User user;

        public MainWindow()
        {
            InitializeComponent();
            logInGrid.Visibility = Visibility.Hidden;
            buttonsMenuGrid.Visibility = Visibility.Hidden;
            iesiriButton.Visibility = Visibility.Hidden; ;
            addUserButton.Visibility = Visibility.Hidden;
            cofetarie = new CofetarieEntities();
        }

        private void logInButton_Click(object sender, EventArgs e)
        {
            if(logInButton.Content == "LogIn")
                logInGrid.Visibility = Visibility.Visible;
            else
            {
                buttonsMenuGrid.Visibility = Visibility.Hidden;
                iesiriButton.Visibility = Visibility.Hidden;
                logInGrid.Visibility = Visibility.Visible;
                NameTextBox.Text = "";
                PassBox.Password = "";
                logIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.AccountSupervisorCircle;
                logInButton.Content = "LogIn";
                usernameTextBlock.Text = "LogIn First";
                user = null;
                addUserButton.Visibility = Visibility.Hidden;
            }
        }

        private void logInAction_Click(object sender, EventArgs e)
        {
            var users = cofetarie.Users;
            user = null;
            foreach (var u in users)
                if (u.Nume == NameTextBox.Text && u.Parola == PassBox.Password)
                    user = u;
            if (user != null)
            {
                buttonsMenuGrid.Visibility = Visibility.Visible;
                iesiriButton.Visibility = Visibility.Visible;
                logInGrid.Visibility = Visibility.Hidden;
                logIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.LogoutVariant;
                logInButton.Content = "LogOut";
                usernameTextBlock.Text = user.Nume;
                if (user.Tip == "admin")
                    addUserButton.Visibility = Visibility.Visible;
            }
            else
            {
                CustomMessageBox mb = new CustomMessageBox();
                mb.Show();
            }
        }

        private void closeLogIn_Click(object sender, EventArgs e)
        {
            NameTextBox.Text = "";
            PassBox.Password = "";
            logInGrid.Visibility = Visibility.Hidden;
        }

        private void closeMenu_Click(object sender, EventArgs e)
        {
            NameTextBox.Text = "";
            PassBox.Password = "";
            logInGrid.Visibility = Visibility.Hidden;
        }
        private void exitButton_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void closeApp_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void addUser(object sender, EventArgs e)
        {
            AddUserView auv = new AddUserView();
            auv.Show();
        }

        private void deleteUser(object sender, EventArgs e)
        {
            DeleteUserView auv = new DeleteUserView();
            auv.Show();
        }

        private void ingredient_Click(object sender, EventArgs e)
        {
            IngridientView ingridientView = new IngridientView(user);
            ingridientView.Show();
        }
        private void listaAprovizionare_Click(object sender, EventArgs e)
        {
            ListaAprovizionareView lav = new ListaAprovizionareView(user);
            lav.Show();
        }

        private void retete_Click(object sender, EventArgs e)
        {
            RetetaView rv = new RetetaView(user);
            rv.Show();
        }

        private void FisaMagazie_Click(object sender, RoutedEventArgs e)
        {
            FisaMagazieView fmv = new FisaMagazieView(user);
            fmv.Show();
        }

        private void RaportIngredientCantitate_Click(object sender, RoutedEventArgs e)
        {
            RaportIngredientCantitate ric = new RaportIngredientCantitate();
            ric.Show();
        }

        private void Cumulativ_Click(object sender, RoutedEventArgs e)
        {
            DocumentCumulativView dcv = new DocumentCumulativView();
            dcv.Show();
        }

        private void iesiri_Click(object sender, RoutedEventArgs e)
        {
            IesiriView iv = new IesiriView(user);
            iv.Show();
        }

        private void PretProductie_Click(object sender, RoutedEventArgs e)
        {
            ProductionPriceView ppv = new ProductionPriceView();
            ppv.Show();
        }
    }
}
