﻿using GestiuneCofetarie.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GestiuneCofetarie
{
    /// <summary>
    /// Interaction logic for FisaDeMagazie.xaml
    /// </summary>
    public partial class FisaDeMagazieView : Window
    {
        FisaDeMagViewModel viewModel;
        public FisaDeMagazieView(User user, Ingredient ingredient)
        {
            InitializeComponent();
            textBlock.Text = ingredient.Denumire;
            viewModel = new FisaDeMagViewModel(ingredient);
            DataContext = viewModel;
        }
    }
}
