﻿using GestiuneCofetarie.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneCofetarie.ViewModel
{
    class FisaDeMagViewModel : BaseViewModel
    {
        ObservableCollection<Magazie> magazie = new ObservableCollection<Magazie>();
        public FisaDeMagViewModel(Ingredient ingredient)
        {
            CofetarieEntities cofetarie = new CofetarieEntities();
            List<Intrare> intrari = new List<Intrare>();
            List<Iesire> iesiri = new List<Iesire>();
            foreach (var i in cofetarie.Intrares)
                if (i.Deleted != 1 && i.IdIngredient == ingredient.Id)
                    intrari.Add(i);
            foreach (var i in cofetarie.Iesires)
                if (i.Deleted != 1 && i.IdIngredient == ingredient.Id)
                    iesiri.Add(i);
            float CMP = 0;
            float stocInitial = 0;
            while (intrari.Count > 0 && iesiri.Count > 0)
            {
                Magazie mag = null;
                if (intrari.ElementAt(0).Data <= iesiri.ElementAt(0).Data)
                {
                    mag = new Magazie(intrari.ElementAt(0), stocInitial, CMP);
                    intrari.RemoveAt(0);
                }
                else
                {
                    mag = new Magazie(iesiri.ElementAt(0), stocInitial, CMP);
                    iesiri.RemoveAt(0);
                }
                magazie.Add(mag);
                CMP = mag.CMP;
                stocInitial = mag.Stoc;
            }
            if(intrari.Count > 0)
                foreach(var i in intrari)
                {
                    Magazie mag = new Magazie(i, stocInitial, CMP);
                    magazie.Add(mag);
                    CMP = mag.CMP;
                    stocInitial = mag.Stoc;
                }
            else
                foreach(var i in iesiri)
                {
                    Magazie magazie = new Magazie(i, stocInitial, CMP);
                    Magazie.Add(magazie);
                    CMP = magazie.CMP;
                    stocInitial = magazie.Stoc;
                }
            OnPropertyChanged("Magazie");
        }

        public ObservableCollection<Magazie> Magazie
        {
            get { return magazie; }
            set { magazie = value; OnPropertyChanged("Magazie"); }
        }
    }
}
