﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GestiuneCofetarie.ViewModel
{
    class FisaMagazieViewModel : BaseViewModel
    {
        private User currentUser;
        private Window currentWindow;
        private CofetarieEntities cofetarie;

        private ICommand generate;

        public FisaMagazieViewModel(User user, Window window)
        {
            currentUser = user;
            currentWindow = window;
            cofetarie = new CofetarieEntities();
            generate = new RelayCommand(Generate);
        }

        public ObservableCollection<Ingredient> FisaMagazie
        {
            get
            {
                ObservableCollection<Ingredient> list = new ObservableCollection<Ingredient>();
                cofetarie = new CofetarieEntities();
                foreach (var l in cofetarie.Ingredients)
                    if(l.Deleted != 1)
                        list.Add(l);
                return list;
            }
        }

        public Ingredient IngredientSelectat
        {
            get;set;
        }

        public ICommand GenerateCommand
        {
            get { return generate; }
        }

        private void Generate()
        {
            if (IngredientSelectat == null)
                MessageBox.Show("Alege un ingredient");
            else
            {
                FisaDeMagazieView fdmv = new FisaDeMagazieView(currentUser, IngredientSelectat);
                fdmv.Show();
                currentWindow.Close();
            }
        }
    }
}
