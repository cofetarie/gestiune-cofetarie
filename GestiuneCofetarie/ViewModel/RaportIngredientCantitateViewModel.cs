﻿using GestiuneCofetarie.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GestiuneCofetarie.ViewModel
{
    class RaportIngredientCantitateViewModel : BaseViewModel
    {
        private ObservableCollection<Reteta> retete = new ObservableCollection<Reteta>();
        private Reteta retetaSelectata = new Reteta();
        private ObservableCollection<RaportCantitate> rezultat = new ObservableCollection<RaportCantitate>();
        private ICommand proceed;
        private CofetarieEntities cofetarie = new CofetarieEntities();
        public RaportIngredientCantitateViewModel()
        {
            foreach (var i in cofetarie.Retetas)
                if (i.Deleted != 1)
                    retete.Add(i);
            proceed = new RelayCommand(Proceed);
        }

        public ObservableCollection<Reteta> Retete
        {
            get
            {
                cofetarie = new CofetarieEntities();
                retete.Clear();
                foreach (var i in cofetarie.Retetas)
                    if (i.Deleted != 1)
                        retete.Add(i);
                return retete;
            }
        }

        public Reteta RetetaSelectata
        {
            get { return retetaSelectata; }
            set { retetaSelectata = value; OnPropertyChanged("RetetaSelectata"); }
        }

        public ObservableCollection<RaportCantitate> Rezultat
        {
            get { return rezultat; }
            set { rezultat = value; OnPropertyChanged("Rezultat"); }
        }

        public string Raport
        { get; set; }
        public ICommand ProceedCommand
        {
            get { return proceed; }
        }

        private void Proceed()
        {
            if (retetaSelectata.Denumire == null)
                MessageBox.Show("Alege o reteta");
            else
                if (Raport == null)
                MessageBox.Show("Introdu raportul de cantitate");
            else
                if (!float.TryParse(Raport, out float raport))
                MessageBox.Show("Raportul trebuie sa fie un numar");
            else
            {
                rezultat.Clear();
                cofetarie = new CofetarieEntities();
                foreach (var i in cofetarie.IngredientCompozities)
                {
                    CompozitieReteta compozitie = cofetarie.CompozitieRetetas.Find(i.IdCompozitie);
                    if (compozitie.IdReteta == retetaSelectata.Id)
                    {
                        RaportCantitate rc = new RaportCantitate(i, i.Cantitate, (raport * i.Cantitate));
                        rezultat.Add(rc);
                    }
                }
                OnPropertyChanged("Rezultat");
            }
        }

    }
}
