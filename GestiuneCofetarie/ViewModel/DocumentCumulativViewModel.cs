﻿using GestiuneCofetarie.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneCofetarie.ViewModel
{
    class DocumentCumulativViewModel : BaseViewModel
    {
        private ObservableCollection<DocumentCumulativ> documentCumulativ = new ObservableCollection<DocumentCumulativ>();
        private DateTime selectedDate = new DateTime();
        public DocumentCumulativViewModel(){}

        public ObservableCollection<DocumentCumulativ> DocumentCumulativ
        {
            get { return documentCumulativ; }
            set { documentCumulativ = value; OnPropertyChanged("DocumentCumulativ"); }
        }

        public decimal Total { get; set; }
        public DateTime SelectedDate
        {
            get{ return selectedDate; }
            set
            {
                selectedDate = value;
                CofetarieEntities cofetarie = new CofetarieEntities();
                documentCumulativ.Clear();
                decimal total = 0;
                foreach (var i in cofetarie.Iesires)
                {
                    //CultureInfo culture = new CultureInfo("en-US");
                    //DateTime data = Convert.ToDateTime(i.Data.ToString(), culture);
                    if (i.Deleted != 1 && i.Data.Month == value.Month && i.Data.Year == value.Year)
                    {
                        documentCumulativ.Add(new DocumentCumulativ(i));
                        total += (decimal) i.Cantitate * i.PretUnitar;
                    }
                }
                Total = total;
                OnPropertyChanged("Total");
                OnPropertyChanged("DocumentCumulativ");
                OnPropertyChanged("SelectedDate");
            }
        }

    }
}
