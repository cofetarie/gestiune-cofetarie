﻿using GestiuneCofetarie.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GestiuneCofetarie.ViewModel
{
    class ProductionPriceViewModel : BaseViewModel
    {
        private ObservableCollection<Reteta> retete = new ObservableCollection<Reteta>();
        private Reteta retetaSelectata = new Reteta();
        private ObservableCollection<RaportPret> rezultat = new ObservableCollection<RaportPret>();
        private ICommand proceed;
        private CofetarieEntities cofetarie = new CofetarieEntities();
        public ProductionPriceViewModel()
        {
            foreach (var i in cofetarie.Retetas)
                if (i.Deleted != 1)
                    retete.Add(i);
            proceed = new RelayCommand(Proceed);
        }

        public ObservableCollection<Reteta> Retete
        {
            get
            {
                cofetarie = new CofetarieEntities();
                retete.Clear();
                foreach (var i in cofetarie.Retetas)
                    if (i.Deleted != 1)
                        retete.Add(i);
                return retete;
            }
        }

        public Reteta RetetaSelectata
        {
            get { return retetaSelectata; }
            set { retetaSelectata = value; OnPropertyChanged("RetetaSelectata"); }
        }

        public ObservableCollection<RaportPret> Rezultat
        {
            get { return rezultat; }
            set { rezultat = value; OnPropertyChanged("Rezultat"); }
        }

        public string Raport
        { get; set; }

        public decimal Total { get; set; }
        public ICommand ProceedCommand
        {
            get { return proceed; }
        }

        private void Proceed()
        {
            if (retetaSelectata.Denumire == null)
                MessageBox.Show("Alege o reteta");
            else
                if (Raport == null)
                MessageBox.Show("Introdu raportul de cantitate");
            else
                if (!float.TryParse(Raport, out float raport))
                MessageBox.Show("Raportul trebuie sa fie un numar");
            else
            {
                rezultat.Clear();
                cofetarie = new CofetarieEntities();
                decimal total = 0;
                foreach (var i in cofetarie.IngredientCompozities)
                {
                    CompozitieReteta compozitie = cofetarie.CompozitieRetetas.Find(i.IdCompozitie);
                    if (compozitie.IdReteta == retetaSelectata.Id)
                    {
                        RaportPret rp = new RaportPret(i, raport);
                        total += rp.Pret;
                        rezultat.Add(rp);
                    }
                }
                OnPropertyChanged("Rezultat");
                Total = total;
                OnPropertyChanged("Total");
            }
        }

    }
}
