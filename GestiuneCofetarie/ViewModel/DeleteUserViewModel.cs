﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GestiuneCofetarie.ViewModel
{
    class DeleteUserViewModel : BaseViewModel
    {
        private ObservableCollection<User> users;
        private User selectedUser;
        private CofetarieEntities cofetarie;
        private ICommand delete;
        private bool enableButton = false;

        public DeleteUserViewModel()
        {
            cofetarie = new CofetarieEntities();
            users = new ObservableCollection<User>();

            foreach (var user in cofetarie.Users)
                if (user.Deleted != 1 && user.Tip != "admin")
                    users.Add(user);

            selectedUser = new User();
            delete = new RelayCommand(Delete);
        }

        public ObservableCollection<User> Users
        {
            get
            {
                cofetarie = new CofetarieEntities();
                users.Clear();
                foreach (var user in cofetarie.Users)
                    if (user.Deleted != 1 && user.Tip != "admin")
                        users.Add(user);
                return users;
            }
            set
            {
                users = value;
                OnPropertyChanged("Users");
            }
        }

        public User SelectedUser
        {
            get
            {
                return selectedUser;
            }
            set
            {
                selectedUser = value;
                enableButton = true;
                OnPropertyChanged("EnableButton");
            }
        }

        public bool EnableButton
        {
            get { return enableButton; }
            set
            {
                enableButton = value;
                OnPropertyChanged("EnableButton");
            }
        }

        public ICommand DeleteUser
        {
            get
            {
                return delete;
            }
        }

        private void Delete()
        {
            using(var context = new CofetarieEntities())
            {
                try
                {
                    var temp = selectedUser;
                    var user = context.Users.Find(temp.Id);

                    user.Deleted = 1;

                    context.SaveChanges();

                    OnPropertyChanged("Users");

                }
                catch(Exception ex) { MessageBox.Show(ex.Message); }
            }
            enableButton = false;
            OnPropertyChanged("EnableButton");
        }

    }
}
