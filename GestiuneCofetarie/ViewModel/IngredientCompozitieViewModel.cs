﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GestiuneCofetarie.ViewModel
{
    class IngredientCompozitieViewModel : BaseViewModel
    {
        private User currentUser;
        private Window currentWindow;
        private string denumireReteta;
        private ObservableCollection<string> compozitii;
        private string compozitieSelectata;
        private ObservableCollection<Ingredient> ingredienteCompozitie;
        private Ingredient ingredientSelectatDinCompozitie;
        private ObservableCollection<Ingredient> ingrediente;
        private Ingredient ingredientSelectat;

        private ICommand transferLeft;
        private ICommand transferRight;
        private ICommand done;

        private CofetarieEntities cofetarie;

        private List<Tuple <Ingredient, string, float> > reteta;

        public IngredientCompozitieViewModel(User user, Window window, ObservableCollection<string> comp, string denumire)
        {
            cofetarie = new CofetarieEntities();
            reteta = new List< Tuple<Ingredient, string, float>>();
            currentUser = user;
            currentWindow = window;
            denumireReteta = denumire;

            compozitii = comp;
            ingredienteCompozitie = new ObservableCollection<Ingredient>();
            ingredientSelectatDinCompozitie = new Ingredient();
            ingrediente = new ObservableCollection<Ingredient>();
            ingredientSelectat = new Ingredient();

            foreach (var i in cofetarie.Ingredients)
                if (i.Deleted == 0)
                    ingrediente.Add(i);

            transferLeft = new RelayCommand(TransferLeft);
            transferRight = new RelayCommand(TransferRight);
            done = new RelayCommand(Done);

        }

        public ObservableCollection<string> Compozitii
        {
            get { return compozitii; }
            set { compozitii = value; OnPropertyChanged("Compozitii"); }
        }

        public string CompozitieSelectata
        {
            get { return compozitieSelectata; }
            set
            {
                compozitieSelectata = value;

                ingredienteCompozitie.Clear();
                foreach (var iterator in reteta)
                    if (iterator.Item2 == value)
                        ingredienteCompozitie.Add(iterator.Item1);
                Cantitate = null;
                OnPropertyChanged("Cantitate");
                OnPropertyChanged("IngredienteCompozitie");

                OnPropertyChanged("CompozitieSelectata");
            }
        }

        public ObservableCollection<Ingredient> Ingrediente
        {
            get { return ingrediente; }
            set { ingrediente = value; OnPropertyChanged("Ingrediente"); }
        }

        public Ingredient IngredientSelectat
        {
            get { return ingredientSelectat; }
            set { ingredientSelectat = value; OnPropertyChanged("IngredientSelectat"); }
        }

        public ObservableCollection<Ingredient> IngredienteCompozitie
        {
            get { return ingredienteCompozitie; }
            set { ingredienteCompozitie = value; OnPropertyChanged("IngredienteCompozitie"); }
        }
        public Ingredient IngredientSelectatDinCompozitie
        {
            get { return ingredientSelectatDinCompozitie; }
            set
            {
                foreach(var iterator in reteta)
                    if(iterator.Item1 == value && iterator.Item2 == compozitieSelectata)
                    {
                        Cantitate = iterator.Item3.ToString();
                        OnPropertyChanged("Cantitate");
                        break;
                    }

                ingredientSelectatDinCompozitie = value;
                OnPropertyChanged("IngredientSelectatDinCompozitie");
            }
        }

        public string Cantitate
        {
            get;set;
        }

        public ICommand TransferLeftCommand
        {
            get { return transferLeft; }
        }

        public ICommand TransferRightCommand
        {
            get { return transferRight; }
        }

        public ICommand DoneCommand
        {
            get { return done; }
        }

        private void TransferLeft()
        {
            if (Cantitate == null || Cantitate == "")
                MessageBox.Show("Introdu o cantitate");
            else
                if (!float.TryParse(Cantitate, out float cantitate))
                MessageBox.Show("Cantitatea trebuie sa fie un numar");
            else
                    if (compozitieSelectata == null)
                MessageBox.Show("Alege o compozitie");
            else
                        if (ingredientSelectat.Denumire == null)
                MessageBox.Show("Alege un ingredient");
            else
            {
                if (ingredienteCompozitie.Contains(ingredientSelectat))
                    MessageBox.Show("Acest ingredient este deja introdus");
                else
                {
                    reteta.Add(new Tuple<Ingredient, string, float>(ingredientSelectat, compozitieSelectata, cantitate));
                    ingredienteCompozitie.Add(ingredientSelectat);
                    OnPropertyChanged("IngredienteCompozitie");
                    Cantitate = null;
                    OnPropertyChanged("Cantitate");
                }
            }
        }

        private void TransferRight()
        {
            if (ingredientSelectatDinCompozitie.Denumire == null)
                MessageBox.Show("Selecteaza un ingredient");
            else
            {
                if (float.TryParse(Cantitate, out float cantitate))
                {
                    reteta.Remove(new Tuple<Ingredient, string, float>(ingredientSelectatDinCompozitie, compozitieSelectata, cantitate));
                    ingredienteCompozitie.Remove(ingredientSelectatDinCompozitie);
                    OnPropertyChanged("IngredienteCompozitie");
                    Cantitate = null;
                    OnPropertyChanged("Cantitate");
                }
                else
                    MessageBox.Show("Cantitatea trebuie sa fie cea initiala");
            }
        }

        private void Done()
        {
            if (reteta.Count() == 0)
                MessageBox.Show("Introdu ingrediente in compozitie");
            else
            if (MessageBox.Show("Done?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                using (var context = new CofetarieEntities())
                {
                    try
                    {
                        Reteta r = new Reteta
                        {
                            Denumire = denumireReteta,
                            AdaugatDe = currentUser.Id,
                            Deleted = 0,
                        };
                        context.Retetas.Add(r);
                        context.SaveChanges();
                        List<CompozitieReteta> listaCompozitii = new List<CompozitieReteta>();
                        foreach (var i in compozitii)
                        {
                            CompozitieReteta compozitieReteta = new CompozitieReteta
                            {
                                Denumire = i,
                                Deleted = 0,
                                Reteta = r,
                            };
                            listaCompozitii.Add(compozitieReteta);
                            context.CompozitieRetetas.Add(compozitieReteta);
                            context.SaveChanges();
                        }
                        List<IngredientCompozitie> listaIngrediente = new List<IngredientCompozitie>();
                        foreach(var i in reteta)
                        {
                            CompozitieReteta compozitie = null;
                            foreach (var c in listaCompozitii)
                                if (c.Denumire == i.Item2)
                                {
                                    compozitie = c;
                                    break;
                                }
                            IngredientCompozitie ingredient = new IngredientCompozitie
                            {
                                IdIngredient = i.Item1.Id,
                                Cantitate = i.Item3,
                                IdCompozitie = compozitie.Id,
                                Deleted = 0,
                                AdaugatDe = currentUser.Id,
                            };
                            context.IngredientCompozities.Add(ingredient);
                        }
                        context.SaveChanges();
                        currentWindow.Close();
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }

            }
        }

    }
}
