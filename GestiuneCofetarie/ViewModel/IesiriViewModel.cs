﻿using GestiuneCofetarie.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GestiuneCofetarie.ViewModel
{
    class IesiriViewModel : BaseViewModel
    {
        private User currentUser;
        private ObservableCollection<Ingredient> ingrediente = new ObservableCollection<Ingredient>();
        private Ingredient ingredientSelectat = new Ingredient();
        private ObservableCollection<GridColumnIesire> iesiri = new ObservableCollection<GridColumnIesire>();
        private GridColumnIesire iesireSelectata = new GridColumnIesire();
        private bool modify = false;
        private bool gridVisibility = false;
        private CofetarieEntities cofetarie = new CofetarieEntities();

        private ICommand add;
        private ICommand update;
        private ICommand delete;
        private ICommand confirm;

        private float calculStoc(Ingredient ingredient)
        {
            float rezultat = 0;
            float deAdunat = 0;
            float deScazut = 0;
            cofetarie = new CofetarieEntities();
            foreach (var i in cofetarie.Intrares)
                if (i.IdIngredient == ingredient.Id)
                    deAdunat += i.Cantitate;
            foreach (var i in cofetarie.Iesires)
                if (i.IdIngredient == ingredient.Id)
                    deScazut += i.Cantitate;
            rezultat = deAdunat - deScazut;
            return rezultat;
        }

        public IesiriViewModel(User user)
        {
            currentUser = user;
            foreach (var ingredient in cofetarie.Ingredients)
                if (ingredient.Deleted != 1)
                {
                    ingrediente.Add(ingredient);
                }

            foreach (var iesire in cofetarie.Iesires)
                if (iesire.Deleted != 1)
                {
                    iesiri.Add(new GridColumnIesire(cofetarie.Ingredients.Find(iesire.IdIngredient).Denumire, iesire));
                }

            add = new RelayCommand(Add);
            update = new RelayCommand(Update);
            delete = new RelayCommand(Delete);
            confirm = new RelayCommand(Confirm);
        }

        public ObservableCollection<Ingredient> Ingredients
        {
            get
            {
                cofetarie = new CofetarieEntities();
                ingrediente.Clear();
                foreach (var ingredient in cofetarie.Ingredients)
                    if (ingredient.Deleted != 1)
                    {
                        ingrediente.Add(ingredient);
                    }
                return ingrediente;
            }
            set
            {
                ingrediente = value;
                OnPropertyChanged("Ingredients");
            }
        }

        public Ingredient IngredientSelectat
        {
            get { return ingredientSelectat; }
            set { ingredientSelectat = value; OnPropertyChanged("IngredientSelectat"); }
        }

        public ObservableCollection<GridColumnIesire> Iesiri
        {
            get
            {
                cofetarie = new CofetarieEntities();
                iesiri.Clear();
                foreach (var iesire in cofetarie.Iesires)
                    if (iesire.Deleted != 1)
                    {
                        iesiri.Add(new GridColumnIesire(cofetarie.Ingredients.Find(iesire.IdIngredient).Denumire, iesire));
                    }
                return iesiri;
            }
        }

        public GridColumnIesire IesireSelectata
        {
            get { return iesireSelectata; }
            set
            {
                iesireSelectata = value;
                OnPropertyChanged("IesireSelectata");
                modify = false;
                gridVisibility = false;
                OnPropertyChanged("GridVisibility");
            }
        }
        public bool GridVisibility
        {
            get { return gridVisibility; }
            set { gridVisibility = value; OnPropertyChanged("GridVisibility"); }
        }

        public string CantitateIesire
        {
            get; set;
        }

        public string Pret
        {
            get; set;
        }

        public string SelectedDate
        {
            get; set;
        }

        public ICommand AddCommand
        {
            get { return add; }
        }

        public ICommand UpdateCommand
        {
            get { return update; }
        }

        public ICommand DeleteCommand
        {
            get { return delete; }
        }

        public ICommand ConfirmCommand
        {
            get { return confirm; }
        }

        private void Add()
        {
            modify = false;
            gridVisibility = true;
            OnPropertyChanged("GridVisibility");
        }

        private void Update()
        {
            if (iesireSelectata.iesire == null)
                MessageBox.Show("Selecteaza o optiune");
            else
            {
                gridVisibility = true;
                OnPropertyChanged("GridVisibility");
                CantitateIesire = iesireSelectata.Cantitate.ToString();
                OnPropertyChanged("CantitateIesire");
                Pret = iesireSelectata.PretUnitar.ToString();
                OnPropertyChanged("Pret");
                modify = true;
                ingredientSelectat = iesireSelectata.iesire.Ingredient;
            }
        }

        private void Delete()
        {
            if (iesireSelectata.Ingredient == null)
                MessageBox.Show("Alegeti ce iesire doriti sa stergeti");
            else
            {
                using (var context = new CofetarieEntities())
                {
                    try
                    {
                        Iesire deSters = context.Iesires.Find(iesireSelectata.iesire.Id);
                        deSters.Deleted = 1;
                        context.SaveChanges();
                        OnPropertyChanged("Iesiri");
                        modify = false;
                        gridVisibility = false;
                        OnPropertyChanged("GridVisibility");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void Confirm()
        {
            if (ingredientSelectat.Denumire == null)
                MessageBox.Show("Alegeti un ingredient");
            else if (CantitateIesire == null)
                MessageBox.Show("Introduceti Cantitatea");
            else if (!float.TryParse(CantitateIesire, out float cantitate))
                MessageBox.Show("Cantitatea trebuie sa fie un numar");
            else if (Pret == null)
                MessageBox.Show("Introduceti pretul unitar");
            else if (!decimal.TryParse(Pret, out decimal pret))
                MessageBox.Show("Pretul trebuie sa fie un numar");
            else if (SelectedDate == null)
                MessageBox.Show("Introduceti data");
            else if (cantitate > calculStoc(ingredientSelectat))
                MessageBox.Show("Stoc insuficient");
            else
            {
                using (var context = new CofetarieEntities())
                {
                    CultureInfo culture = new CultureInfo("en-US");
                    DateTime data = Convert.ToDateTime(SelectedDate, culture);
                    if (modify)
                    {
                        var deModificat = context.Iesires.Find(iesireSelectata.iesire.Id);
                        deModificat.Cantitate = cantitate;
                        deModificat.PretUnitar = pret;
                        deModificat.Data = data;
                        deModificat.AdaugatDe = currentUser.Id;
                        deModificat.IdIngredient = ingredientSelectat.Id;
                        context.SaveChanges();
                        OnPropertyChanged("Iesiri");
                    }
                    else
                        try
                        {
                            var iesire = new Iesire
                            {
                                IdIngredient = ingredientSelectat.Id,
                                Cantitate = cantitate,
                                PretUnitar = pret,
                                Data = data,
                                AdaugatDe = currentUser.Id,
                                Deleted = 0,
                            };
                            context.Iesires.Add(iesire);
                            context.SaveChanges();
                            OnPropertyChanged("Iesiri");
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
                modify = false;
                gridVisibility = false;
                OnPropertyChanged("GridVisibility");
            }
        }

    }
}
