﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GestiuneCofetarie.ViewModel
{
    class CompozitieRetetaViewModel : BaseViewModel
    {
        private User currentUser;
        private Window currentWindow;
        private ObservableCollection<string> compozitie;
        private string compozitieSelectata;
        private bool panelVisibility;
        private bool enableButton = false;

        private ICommand adaugaCompozitie;
        private ICommand add;
        private ICommand delete;
        private ICommand next;
        public CompozitieRetetaViewModel(User user, Window window)
        {
            currentUser = user;
            currentWindow = window;
            compozitie = new ObservableCollection<string>();
            panelVisibility = false;

            adaugaCompozitie = new RelayCommand(AdaugaCompozitie);
            add = new RelayCommand(Add);
            delete = new RelayCommand(Delete);
            next = new RelayCommand(Next);
        }

        public ObservableCollection<string> Compozitie
        {
            get { return compozitie; }
            set { compozitie = value; OnPropertyChanged("Compozitie"); }
        }

        public string CompozitieSelectata
        {
            get { return compozitieSelectata; }
            set { compozitieSelectata = value; OnPropertyChanged("CompozitieSelectata"); enableButton = true;
                OnPropertyChanged("EnableButton");
            }
        }

        public bool PanelVisibility
        {
            get { return panelVisibility; }
            set { panelVisibility = value; OnPropertyChanged("PanelVisibility"); }
        }

        public bool EnableButton
        {
            get { return enableButton; }
            set
            {
                enableButton = value;
                OnPropertyChanged("EnableButton");
            }
        }
        public string DenumireReteta
        {
            get;set;
        }

        public string DenumireCompozitie
        {
            get;set;
        }

        public ICommand AddCommand
        {
            get { return add; }
        }

        public ICommand AdaugaCompozitieCommand
        {
            get { return adaugaCompozitie; }
        }

        public ICommand DeleteCommand
        {
            get { return delete; }
        }

        public ICommand NextCommand
        {
            get { return next; }
        }

        private void Add()
        {
            panelVisibility = true;
            OnPropertyChanged("PanelVisibility");
        }
        private void AdaugaCompozitie()
        {
            if (DenumireCompozitie == "" || DenumireCompozitie == null)
                MessageBox.Show("Adauga un nume compozitiei");
            else
            {
                bool ok = true;
                foreach(var c in compozitie)
                    if(c == DenumireCompozitie)
                    {
                        MessageBox.Show("Aceasta compozitie exista deja");
                        ok = false;
                    }
                if (ok)
                {
                    compozitie.Add(DenumireCompozitie);
                    OnPropertyChanged("Compozitie");
                    DenumireCompozitie = null;
                    OnPropertyChanged("DenumireCompozitie");
                    panelVisibility = false;
                    OnPropertyChanged("PanelVisibility");
                }
            }
        }

        private void Delete()
        {
            compozitie.Remove(compozitieSelectata);
            OnPropertyChanged("Compozitie");
            enableButton = false;
            OnPropertyChanged("EnableButton");
        }

        private void Next()
        {
            if (DenumireReteta == null || DenumireReteta == "")
                MessageBox.Show("Denumeste reteta");
            else
                if (compozitie.Count < 1)
                    MessageBox.Show("Adauga cel putin o compozitie");
            else
            {
                CofetarieEntities cofetarie = new CofetarieEntities();
                var retete = cofetarie.Retetas;
                bool ok = true;
                foreach(var i in retete)
                    if(DenumireReteta == i.Denumire)
                    {
                        ok = false;
                        break;
                    }
                if (ok)
                {
                    currentWindow.Close();
                    IngredientCompozitieView icv = new IngredientCompozitieView(currentUser, compozitie, DenumireReteta);
                    icv.Show();
                }
                else
                    MessageBox.Show("Aceasta reteta exista deja");
            }
        }

    }
}
