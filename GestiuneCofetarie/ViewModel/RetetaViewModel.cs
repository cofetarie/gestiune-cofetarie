﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GestiuneCofetarie.ViewModel
{
    class RetetaViewModel : BaseViewModel
    {
        private User currentUser;
        private CofetarieEntities cofetarie;
        private ObservableCollection<Reteta> retete;
        private Reteta retetaSelectata;
        private bool enableButton = false;

        private ICommand add;
        private ICommand delete;

        public RetetaViewModel(User user)
        {
            currentUser = user;
            cofetarie = new CofetarieEntities();
            retetaSelectata = new Reteta();
            retete = new ObservableCollection<Reteta>();

            foreach (var reteta in cofetarie.Retetas)
                if (reteta.Deleted != 1)
                    retete.Add(reteta);

            add = new RelayCommand(Add);
            delete = new RelayCommand(Delete);
        }

        public ObservableCollection<Reteta> Retete
        {
            get
            {
                cofetarie = new CofetarieEntities();
                retete.Clear();
                foreach (var reteta in cofetarie.Retetas)
                    if (reteta.Deleted != 1)
                        retete.Add(reteta);
                return retete;
            }
            set
            {
                retete = value;
                OnPropertyChanged("Retete");
            }
        }

        public Reteta RetetaSelectata
        {
            get { return retetaSelectata; }
            set { retetaSelectata = value; OnPropertyChanged("RetetaSelectata"); enableButton = true;
                OnPropertyChanged("EnableButton");}
        }

        public bool EnableButton
        {
            get { return enableButton; }
            set
            {
                enableButton = value;
                OnPropertyChanged("EnableButton");
            }
        }

        public ICommand AddCommand
        {
            get { return add; }
        }

        public ICommand DeleteCommand
        {
            get { return delete; }
        }

        private void Add()
        {
            CompozitieRetetaView crv = new CompozitieRetetaView(currentUser);
            crv.Show();
        }

        private void Modify()
        {
            
        }

        private void Delete()
        {
            using (var context = new CofetarieEntities())
            {
                if (MessageBox.Show("Doresti sa stergi " + retetaSelectata.Denumire + "?", "Opertie Stergere Reteta", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    try
                    {
                        var reteta = context.Retetas.Find(retetaSelectata.Id);
                        reteta.Deleted = 1;
                        foreach (var compozitie in context.CompozitieRetetas)
                        {
                            if (compozitie.IdReteta == reteta.Id)
                                compozitie.Deleted = 1;
                            foreach (var ingredient in context.IngredientCompozities)
                                if (ingredient.IdCompozitie == compozitie.Id)
                                    ingredient.Deleted = 1;
                        }
                        context.SaveChanges();
                        OnPropertyChanged("Retete");
                        enableButton = false;
                        OnPropertyChanged("EnableButton");
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

    }
}
