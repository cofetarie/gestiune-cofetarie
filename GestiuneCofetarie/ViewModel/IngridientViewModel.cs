﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GestiuneCofetarie.ViewModel
{
    class IngridientViewModel : BaseViewModel
    {
        private User currentUser;
        private ObservableCollection<Ingredient> ingredients;
        private Ingredient selectedIngredient;
        private CofetarieEntities cofetarie;
        private bool gridVisibility;
        private bool modifyIngredient;

        private ICommand add;
        private ICommand update;
        private ICommand delete;
        private ICommand confirm;

        public IngridientViewModel(User user)
        {
            cofetarie = new CofetarieEntities();
            ingredients = new ObservableCollection<Ingredient>();
            selectedIngredient = new Ingredient();

            foreach (var ingredient in cofetarie.Ingredients)
                if (ingredient.Deleted != 1)
                    ingredients.Add(ingredient);

            add = new RelayCommand(Add);
            update = new RelayCommand(Update);
            delete = new RelayCommand(Delete);
            confirm = new RelayCommand(Confirm);
            gridVisibility = false;
            modifyIngredient = false;
            currentUser = user;
        }

        public ObservableCollection<Ingredient> Ingredients
        {
            get
            {
                cofetarie = new CofetarieEntities();
                ingredients.Clear();
                foreach (var ingredient in cofetarie.Ingredients)
                    if (ingredient.Deleted != 1)
                        ingredients.Add(ingredient);
                return ingredients;
            }
            set
            {
                ingredients = value;
                OnPropertyChanged("Ingredients");
            }
        }

        public Ingredient SelectedIngredient
        {
            get { return selectedIngredient; }
            set { selectedIngredient = value; OnPropertyChanged("SelectedIngredient"); }
        }

        public bool GridVisibility
        {
            get { return gridVisibility; }
            set { gridVisibility = value; OnPropertyChanged("GridVisibility"); }
        }

        public string DenumireIngredient
        {
            get; set;
        }

        public ICommand AddCommand
        {
            get { return add; }
        }

        public ICommand UpdateCommand
        {
            get { return update; }
        }

        public ICommand DeleteCommand
        {
            get { return delete; }
        }

        public ICommand ConfirmCommand
        {
            get { return confirm; }
        }

        private void Add()
        {
            DenumireIngredient = "";
            OnPropertyChanged("DenumireIngredient");
            gridVisibility = true;
            OnPropertyChanged("GridVisibility");
            modifyIngredient = false;
        }

        private void Update()
        {
            if (selectedIngredient.Denumire == null)
                MessageBox.Show("Alege Ingredient");
            else
            {
                gridVisibility = true;
                OnPropertyChanged("GridVisibility");
                modifyIngredient = true;
                DenumireIngredient = selectedIngredient.Denumire;
                OnPropertyChanged("DenumireIngredient");
            }
        }

        private void Delete()
        {
            if (selectedIngredient.Denumire == null)
                MessageBox.Show("Alege Ingredient");
            else
            {
                using (var context = new CofetarieEntities())
                {
                    if (MessageBox.Show("Doresti sa stergi " + selectedIngredient.Denumire + "?", "Opertie Stergere Ingredient", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    try
                    {
                        var ingredient = context.Ingredients.Find(selectedIngredient.Id);
                        ingredient.Deleted = 1;
                        context.SaveChanges();
                        OnPropertyChanged("Ingredients");
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void Confirm()
        {
             using (var context = new CofetarieEntities())
             {
                 try
                 {
                     bool ok = true;
                     var ingredients = context.Ingredients;
                     foreach(var i in ingredients)
                         if(i.Denumire == DenumireIngredient)
                         {
                             ok = false;
                             MessageBox.Show("Acest ingredient exista deja");
                             break;
                         }
                    if (ok)
                        if (DenumireIngredient != "")
                        {
                            var ingredient = new Ingredient();
                            if (modifyIngredient)
                            {
                                ingredient = context.Ingredients.Find(selectedIngredient.Id);
                                ingredient.Denumire = DenumireIngredient;
                            }
                            else
                            {
                                ingredient = new Ingredient
                                {
                                    Denumire = DenumireIngredient,
                                    AdaugatDe = currentUser.Id,
                                    Deleted = 0,
                                };
                                context.Ingredients.Add(ingredient);
                            }
                            context.SaveChanges();
                            OnPropertyChanged("Ingredients");
                            gridVisibility = false;
                            OnPropertyChanged("GridVisibility");
                        }
                        else
                            MessageBox.Show("Introdu denumirea ingredientului");
                 }
                 catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

    }
}
