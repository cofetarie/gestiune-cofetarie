﻿using GestiuneCofetarie.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GestiuneCofetarie.ViewModel
{
    class ListaAprovizionareViewModel : BaseViewModel
    {
        private User currentUser;
        private ObservableCollection<Stoc> stoc;
        private Stoc selectedIngredient;
        private ObservableCollection<GridColumnIntrare> intrari;
        private GridColumnIntrare intrareSelectata;
        private CofetarieEntities cofetarie;
        private bool gridVisibility;
        private bool modify;

        private ICommand add;
        private ICommand update;
        private ICommand delete;
        private ICommand confirm;
        private ICommand validate;

        private float calculStoc(Ingredient ingredient)
        {
            float rezultat = 0;
            float deAdunat = 0;
            float deScazut = 0;
            cofetarie = new CofetarieEntities();
            foreach (var i in cofetarie.Intrares)
                if (i.IdIngredient == ingredient.Id)
                    deAdunat += i.Cantitate;
            foreach (var i in cofetarie.Iesires)
                if (i.IdIngredient == ingredient.Id)
                    deScazut += i.Cantitate;
            rezultat = deAdunat - deScazut;
            return rezultat;
        }

        public ListaAprovizionareViewModel(User user)
        { 
            cofetarie = new CofetarieEntities();
            stoc = new ObservableCollection<Stoc>();
            selectedIngredient = new Stoc();
            intrari = new ObservableCollection<GridColumnIntrare>();
            intrareSelectata = new GridColumnIntrare();

            foreach (var ingredient in cofetarie.Ingredients)
                if (ingredient.Deleted != 1)
                {
                    stoc.Add(new Stoc(ingredient.Denumire, calculStoc(ingredient)));
                }

            foreach(var intrare in cofetarie.IntrareAprovizionares)
                if(intrare.Deleted != 1)
                {
                    intrari.Add(new GridColumnIntrare(cofetarie.Ingredients.Find(intrare.IdIngredient).Denumire, intrare));
                }

            add = new RelayCommand(Add);
            update = new RelayCommand(Update);
            delete = new RelayCommand(Delete);
            confirm = new RelayCommand(Confirm);
            validate = new RelayCommand(Validate);
            gridVisibility = false;
            modify = false;
            currentUser = user;
        }

        public ObservableCollection<Stoc> Ingredients
        {
            get
            {
                cofetarie = new CofetarieEntities();
                stoc.Clear();
                foreach (var ingredient in cofetarie.Ingredients)
                    if (ingredient.Deleted != 1)
                        stoc.Add(new Stoc(ingredient.Denumire, calculStoc(ingredient)));
                return stoc;
            }
            set
            {
                stoc = value;
                OnPropertyChanged("Ingredients");
            }
        }

        public Stoc SelectedIngredient
        {
            get { return selectedIngredient; }
            set { selectedIngredient = value; OnPropertyChanged("SelectedIngredient"); }
        }

        public ObservableCollection<GridColumnIntrare> Intrari
        {
            get
            {
                cofetarie = new CofetarieEntities();
                intrari.Clear();
                foreach (var intrare in cofetarie.IntrareAprovizionares)
                    if (intrare.Deleted != 1)
                    {
                        intrari.Add(new GridColumnIntrare(cofetarie.Ingredients.Find(intrare.IdIngredient).Denumire, intrare));
                    }
                return intrari;
            }
        }

        public GridColumnIntrare IntrareSelectata
        {
            get { return intrareSelectata; }
            set
            {
                intrareSelectata = value;
                OnPropertyChanged("IntrareSelectata");
                modify = false;
                gridVisibility = false;
                OnPropertyChanged("GridVisibility");
            }
        }

        public bool GridVisibility
        {
            get { return gridVisibility; }
            set { gridVisibility = value; OnPropertyChanged("GridVisibility"); }
        }

        public string CantitateIntrare
        {
            get; set;
        }

        public string Pret
        {
            get; set;
        }

        public string SelectedDate
        {
            get; set;
        }

        public ICommand AddCommand
        {
            get { return add; }
        }

        public ICommand UpdateCommand
        {
            get { return update; }
        }

        public ICommand DeleteCommand
        {
            get { return delete; }
        }

        public ICommand ConfirmCommand
        {
            get { return confirm; }
        }
        public ICommand ValidateCommand
        {
            get { return validate; }
        }

        private void Add()
        {
            modify = false;
            gridVisibility = true;
            OnPropertyChanged("GridVisibility");
        }

        private void Update()
        {
            if (intrareSelectata.intrare == null)
                MessageBox.Show("Selecteaza o intrare");
            else
            {
                gridVisibility = true;
                OnPropertyChanged("GridVisibility");
                CantitateIntrare = intrareSelectata.Cantitate.ToString();
                OnPropertyChanged("CantitateIntrare");
                Pret = intrareSelectata.PretUnitar.ToString();
                OnPropertyChanged("Pret");
                SelectedDate = intrareSelectata.intrare.Data.ToString();
                OnPropertyChanged("SelectedDate");
                modify = true;
                cofetarie = new CofetarieEntities();
                foreach (var i in stoc)
                    if (i.Ingredient == intrareSelectata.Ingredient)
                    {
                        selectedIngredient = i;
                        break;
                    }
            }
        }

        private void Delete()
        {
            if (intrareSelectata.Ingredient == null)
                MessageBox.Show("Alegeti ce intrare doriti sa stergeti");
            else
            {
                int index = intrari.IndexOf(intrareSelectata);
                using (var context = new CofetarieEntities())
                {
                    try
                    {
                        IntrareAprovizionare deSters = context.IntrareAprovizionares.Find(intrareSelectata.intrare.Id);
                        deSters.Deleted = 1;
                        context.SaveChanges();
                        OnPropertyChanged("Intrari");
                        modify = false;
                        gridVisibility = false;
                        OnPropertyChanged("GridVisibility");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void Confirm()
        {
            if (selectedIngredient.Ingredient == null)
                MessageBox.Show("Alegeti un ingredient");
            else if (CantitateIntrare == null)
                MessageBox.Show("Introduceti Cantitatea");
            else if (!float.TryParse(CantitateIntrare, out float cantitate))
                MessageBox.Show("Cantitatea trebuie sa fie un numar");
            else if (Pret == null)
                MessageBox.Show("Introduceti pretul unitar");
            else if (!decimal.TryParse(Pret, out decimal pret))
                MessageBox.Show("Pretul trebuie sa fie un numar");
            else if (SelectedDate == null)
                MessageBox.Show("Introduceti data");
            else
            {
                using (var context = new CofetarieEntities())
                {
                    CultureInfo culture = new CultureInfo("en-US");
                    DateTime data = Convert.ToDateTime(SelectedDate, culture);
                    if (modify)
                    {
                        var deModificat = context.IntrareAprovizionares.Find(intrareSelectata.intrare.Id);
                        deModificat.Cantitate = cantitate;
                        deModificat.PretUnitar = pret;
                        deModificat.Data = data;
                        deModificat.AdaugatDe = currentUser.Id;
                        foreach (var i in context.Ingredients)
                            if (i.Denumire == selectedIngredient.Ingredient)
                                deModificat.IdIngredient = i.Id;
                        context.SaveChanges();
                        OnPropertyChanged("Intrari");
                    }
                    else
                        try
                        {
                            int idIngredient = -1;
                            foreach(var i in context.Ingredients)
                                if(selectedIngredient.Ingredient == i.Denumire)
                                {
                                    idIngredient = i.Id;
                                    break;
                                }
                            var intrare = new IntrareAprovizionare
                            {
                                IdIngredient = idIngredient,
                                Cantitate = cantitate,
                                PretUnitar = pret,
                                Data = data,
                                AdaugatDe = currentUser.Id,
                                Deleted = 0,
                            };
                            context.IntrareAprovizionares.Add(intrare);
                            context.SaveChanges();
                            OnPropertyChanged("Intrari");
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
                modify = false;
                gridVisibility = false;
                OnPropertyChanged("GridVisibility");
            }
        }

        private void Validate()
        {
            if (intrareSelectata.Ingredient == null)
                MessageBox.Show("Alegeti ce intrare doriti sa validati");
            else
            {
                if (MessageBox.Show("Validate?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    using (var context = new CofetarieEntities())
                    {
                        try
                        {
                            IntrareAprovizionare deValidat = context.IntrareAprovizionares.Find(intrareSelectata.intrare.Id);
                            deValidat.Deleted = 1;
                            Intrare intrare = new Intrare
                            {
                                AdaugatDe = currentUser.Id,
                                Cantitate = deValidat.Cantitate,
                                Data = deValidat.Data,
                                PretUnitar = deValidat.PretUnitar,
                                IdIngredient = deValidat.IdIngredient,
                                Deleted = 0,
                            };
                            context.Intrares.Add(intrare);
                            context.SaveChanges();
                            OnPropertyChanged("Intrari");

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
            }
        }

    }
}
