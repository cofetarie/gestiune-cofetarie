﻿using GestiuneCofetarie.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GestiuneCofetarie
{
    /// <summary>
    /// Interaction logic for IngredientCompozitieView.xaml
    /// </summary>
    public partial class IngredientCompozitieView : Window
    {
        private IngredientCompozitieViewModel icvm;
        public IngredientCompozitieView(User user, ObservableCollection<string> compozitii, string denumire)
        {
            InitializeComponent();
            icvm = new IngredientCompozitieViewModel(user, this, compozitii, denumire);
            DataContext = icvm;
        }
    }
}
