﻿using GestiuneCofetarie.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GestiuneCofetarie
{
    /// <summary>
    /// Interaction logic for RetetaView.xaml
    /// </summary>
    public partial class RetetaView : Window
    {
        RetetaViewModel rvm;
        public RetetaView(User user)
        {
            InitializeComponent();
            rvm = new RetetaViewModel(user);
            DataContext = rvm;
        }
        public void CloseClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
