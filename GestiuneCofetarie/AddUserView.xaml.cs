﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GestiuneCofetarie
{
    /// <summary>
    /// Interaction logic for AddUserView.xaml
    /// </summary>
    public partial class AddUserView : Window
    {
        public AddUserView()
        {
            InitializeComponent();
        }

        private void addUser(object sender, EventArgs e)
        {
            if (password.Password != confirmPassword.Password)
                confirmPassword.BorderBrush = Brushes.Red;
            else
            {
                var converter = new System.Windows.Media.BrushConverter();
                confirmPassword.BorderBrush = (Brush)converter.ConvertFromString("#1d83c5");
                try
                {
                    using (var context = new CofetarieEntities())
                    {
                        var users = context.Users;
                        bool ok = true;
                        foreach (var u in users)
                            if (Username.Text == u.Nume)
                            {
                                Username.BorderBrush = Brushes.Red;
                                ok = false;
                                break;
                            }
                        if(ok)
                        {
                            var user = new User
                            {
                                Nume = Username.Text,
                                Parola = password.Password,
                                Tip = "angajat",
                                Deleted = 0,
                            };
                            context.Users.Add(user);
                            context.SaveChanges();
                            this.Close();
                        }
                    }
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }

            }
        }

        private void close(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
